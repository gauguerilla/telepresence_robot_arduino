var express = require('express');
var app = express();
var cors = require('cors')
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.use(cors());
app.use(nocache);

const PORT = 5000;
app.use("/",express.static(__dirname + "/"))

/*app.get('/client', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
*/

function nocache(req, res, next) {
  
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}



io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on("start",(payload)=>{
    console.log("got start event:",payload, "from ", socket.conn.id);
    
      console.log("will broadcast to all clients");
      activeClientId = socket.conn.id;
      socket.broadcast.emit("start", payload); // to all but sender
      //io.emit("start",payload);
  })

  socket.on("end",(payload)=>{
    console.log("got end event:",payload, "from ", socket.conn.id);
    console.log("will broadcast to all clients");
    socket.broadcast.emit("end", payload); // to all but sender
    //io.emit("end",payload);
  })

  socket.on("director",(payload,val)=>{
    console.log("server got director message", payload,val);
    io.emit("director",payload,val)
  })

});

http.listen(PORT, () => {
  console.log('listening on *:'+PORT);

  setTimeout(() => {
    console.log('emit reload-client.');
    //io.emit("reload-client");    
  }, 1000);

});


/*
let count = 1;
setInterval(() => {
    count++;
    io.emit("displayContent",
        {
          command:"backward"
        }
    )
    console.log("send",count);
    
}, 5000);
*/