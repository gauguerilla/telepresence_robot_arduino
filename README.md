# Telepresence Robot

A working proof of concept for a telepresence robot based on Arduino compatible microcontrollers and ESP modules.

<img src="./doc/robo_front.jpeg" height="300px"><img src="./doc/robo_back.jpeg" height="300px">

part of <a href="https://philipsteimel.de/copresence/">this real life game art project</a>

Colaborators needed!

Contributions welcome!

[[_TOC_]]

## Movement:

Movement is done by wheels attached to two 360 degree servos on `Pins 6` and `Pin 7` of an Arduino Uno or another compatible 5V microcontroller. 

Telepresence feature is realized by a Smartphone running the video conference software of your choice. A third Servo is attached to `Pin 5` to control up and down movements of the smartphone/camera.

## Power

The System is powered by a USB powerbank with 2 USB outputs. One is running the motor controlling unit, the other one is powering the ESP Module.

## Remote Control / Arduino Microcontroller / WebUI


### Prerequisites
 You need a basic understanding of how to program Microcontrollers with the <a href="https://www.arduino.cc">Arduino</a> c++ environment. 

For Variant 2 (Socket.io) you also need some basic understanding of HTML/JS and nodejs.

### General Overview
Remote control is realized via an ESP module in client mode on wifi (credentials atm hardcoded in the code).

The two microcontollers communicate via a hardwired serial connection. The ESP module sends string commands over the serial everytime it receives a network command (HTTP in version 1 or Socket.io event in Version 2) on one of its endpoints.

There are two systems, both controlled differently:

### Version 1: Chunky Moves and HTTP Requests
For this version:
- from `Arduino_ESP8266_Network_Bridges` install `HTTP_to_Serial_Bridge.ino` onto the ESP module.
- from `Arduino_Driver_SerialInput` install telepresence_robot_chunky_moves.ino onto the motor driving Microcontroller (in my case an ARDUINO UNO)

 In this version the ESP module exposes a html button interface to the local network. Inside of the local network the ESPs port 80 must be exposed to the outside world (WAN). Either via port forwarding of your router or via a tunneling service like <a href="https://ngrok.com/">ngrok</a>.

 In this version the motor controlling Arduino board will make the robot move for 1 second for each received HTTP command and then stop. This way users have to retrigger every second, to get the robot to move. 

### Version 2: More fluid movement and SOCKET.IO
For this version:
- from `Arduino_ESP8266_Network_Bridges` install SocketIO_to_Serial_Bridge.ino onto the ESP module. (you will have to install all needed libraries as well)
- from `Arduino_Driver_SerialInput` install install telepresence_robot_fluent_moves.ino onto the motor driving Microcontroller (in my case an ARDUINO UNO)
- for the WebUI: 
    ```bash
    cd webUI_socketIO;
    npm install;
    npm start;
    ```
    open a browser and go to `<yourip>:5000/socketioclient.html`

In this version the ESP module connects as a client to a <a href="https://socket.io">socket.io</a> server on the network. Inside of the local network the ESPs port 5000 must be exposed to the outside world (WAN). Either via port forwarding of your router or via a tunneling service like <a href="https://ngrok.com/">ngrok</a>.

In this version as long as no "end" event reaches the ESP module from the Socket.io server, the motor controlling Arduino board will continue moving. This way you can implement a User Interface where holding a button down will keep the robot rolling.

The WebUI and the control server can be found at `webUI_socketIO`. The control UI is hosted on port 5000 as single file `socketioclient.html`. The server itself is a nodejs express server using socketio and express to manage all connections. 

The robot as well as clients connect to the server as socket.io clients.

The WifiManger will open an AccessPoint if it cannot reach its' known network. Log into the Access Point "Copresence" and provide your wifi credentials and the IP you want the socket.io server to run on. Then hit SAVE. The ESP microcontroller will connect to the provided network and will try to connect via socketIO.

## Hardware / Build notes / Issues

This built is heavily inspired by <a href="https://www.instructables.com/Telepresence-Robot-2/">this instructable</a> by randofo.

the modified partlist for my build looks a little different though: see <a href="./hardware/BOM.md">/hardware/BOM.md</a>

### Base / Chassis
In particular the chassis (base) can be build pretty exactly to their <a href="https://www.instructables.com/Telepresence-Robot-Basic-Platform-Part-1/"> instructions for the basic platform</a> of the robot. 
I replaced the AA battery packs with a USB power bank with 2-3 USB sockets. 

Both methods should still be possible depending on your materials. The upside to a power bank was to me, that I

- + dont have to solder all the power connectors.
- + can easliy charge it
- + later also charge the mobile phone (in case i have 3 USB sockets available)

The downside is

- - you must use servos that can run on the 5V that the power bank provides. In my case that worked fine (I used <a href="http://www.goteckrc.com/ProductShow.asp?ID=14">GS-3360BB</a> that run between 4.8V and 6V)

I also did not use the base sliders listed but instead used some lego wheels I had lying around. The lego wheels worked great to stabilize the base when driving forward and backwards (even gave them better chances to drive over small barriers). But they added a lot of resistance to left and right turns. 

In hindsight I would recommend to stick with the sliders as described by the instructable **or** use <a href="https://duckduckgo.com/?q=ball+casters&atb=v131-1&iax=images&ia=images">ball casters</a> to do the job.

<img src="./doc/chassis_bottom.jpeg" height="300px">
<img src="./doc/chassis_open.jpeg" height="300px">
<img src="./doc/powerbank.jpeg" height="300px">

### Camera Holder 

The robot needs to be able to look up and down. This is where the third servo comes into the game: It needs to be attached to a selfie-stick, a tripod or a monopod that fits your robots chassie.

The selfie stick used by randofos instructable was not available in my country. With its flexible top it seemed overly specific. So I used a flexible tripod and designed a 3D model of an adapter that connects one of the servos with a standard camera plate via the 1/4" screw usually used on tripods, selfie-sticks and other camera equipment. The adapter is designed to be 3D printed and can be found at `/hardware/servoholder.stl`. It is a simple adapter, that could also be made from 2 wood pieces glued together and a <a href="https://duckduckgo.com/?q=1%2F4%22+thread+cutting+tool&atb=v131-1&ia=web">1/4" thread tapping tool</a>

<img src="./doc/adapter.jpeg" height="300px">

This way you can attach it to any tripod, monopod or cameraplate. 

To hold the smartphone on the servo I used the detachable camera clamp that came with my mini tripod and and attached it to the servo with mini zip ties. Depending on your tripod/selfiestick model, you might have to print the clamp part from one of the many smartphone holders on thingiverse or improvize with a smartphone holder you can get your hands on. In the future I hope to design or find a universal holder that fits a servo.

<img src="./doc/servo_smartphone_birdsview.jpeg" height="300px">
<img src="./doc/servo_smartphone_closeup.jpeg" height="300px">
<img src="./doc/servo_smartphone_sideview.jpeg" height="300px">



### Wiring Diagram

<img src="doc/robot_wiring_bb.png" alt="wiring diagram">

## To Dos
 - [x] add Hardware documentation as is
     - [ ] texts
     - [x] photos
     - [x] STL files
 - [ ] modify to easily switch between ESP32 and ESP8266 usage
 - [ ] document ESP bridges HTTP endpoints
 - [ ] document motor controller's Serial bridge endpoints
 - [ ] rebuild project in street ready version. e.g. based on a hoverboard.
