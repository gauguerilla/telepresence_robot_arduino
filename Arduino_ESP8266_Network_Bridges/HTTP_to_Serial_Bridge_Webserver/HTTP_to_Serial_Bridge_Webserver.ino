/*
  Wifi Webserver parts based on code from bitluni
  Released under Creative Commons Attribution 4.0
  by bitluni 2016
  https://creativecommons.org/licenses/by/4.0/
  Attribution means you can use it however you like as long you
  mention that it's base on my stuff.
  I'll be pleased if you'd do it by sharing http://youtube.com/bitlunislab

  modified by Philip Steimel from dingsda.org and machinaex.com

  TODO: get /ECHO Serial response and serve on HTTP endpoint (via regular /ECHO and Serial read)

  TODO: adapt to ESP32 libraries as well and build in ifdef condition / config.h file
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

//#include <Adafruit_NeoPixel.h>


// Serial receive vars 
char receivedChar;
const byte numChars = 64; // max number of chars that can be read //used to be 32
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;
 
//wifi connection variables

char ssid[] = "Erdmaennchen";
char password[] = "WhatwouldLesliewifi?";

ESP8266WebServer server(80);

boolean wifiConnected = false;


const String redirect1 = "<!DOCTYPE html><html><head><!-- HTML meta refresh URL redirection --><meta http-equiv=\"refresh\" content=\"0; url=http://";
const String redirect2 = "\"></head><body></body></html>";
String redirect = ""; // getting filled with IP
String lastResponseFromEcho = "nothing yet";

void handleRoot() 
{
  String message = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"></head>";
  message += "<style>.button {background-color: #4CAF50;border:none;color:white;padding:15px 32px;text-align: center;text-decoration:none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;}.button2 {background-color: #008CBA;}.button3 {background-color: #f44336;}.button4 {background-color: #e7e7e7; color: black;}.button5 {background-color: #555555;}</style>";
  message += "<body style='font-family: sans-serif; font-size: 18px'><div align=\"center\"><h3>machina eX'</h3><br><h2>Telepresence Robot Service</h2>";
  message += "<!--<a href=\"/configpage\" align=\"right\"><p style=\"font-size:0.3em\">config</a>--><br>";
  //message += "standalone on/off <input type=\"checkbox\" checked onclick=\"checkboxClicked(this)\"><br>";
  //message += "sound on/off <input type=\"checkbox\" checked onclick=\"checkboxSoundClicked(this)\"><br>";
  message += "<iframe allow=\"camera; microphone; fullscreen; display-capture\" src=\"https://8x8.vc/uniawm/gauguerilla\" style=\"height: 700px; width: 700px; border: 0px;\"></iframe>";
  message += "<script src='https://meet.jit.si/external_api.js'></script><script>const domain = 'meet.jit.si';const api = new JitsiMeetExternalAPI(domain, options);</script>";
  message += "<button class=\"button\" onclick=\"ajax('/FORWARD')\">FORWARD</button><br><br>";
  message += "<button class=\"button\" onclick=\"ajax('/LEFT')\">LEFT</button>";
  message += "<button class=\"button\" onclick=\"ajax('/RIGHT')\">RIGHT</button><br><br>";
  message += "<button class=\"button\" onclick=\"ajax('/BACKWARD')\">BACKWARD</button><br><br>";
  message += "<button class=\"button\" onclick=\"ajax('/UP')\">UP</button>";
  message += "<button class=\"button\" onclick=\"ajax('/DOWN')\">DOWN</button><br><br>";
  message += "<button class=\"button\" onclick=\"ajax('/BEEP?note=1000&length=500')\">BEEP</button>";
  //message += "<button class=\"button\" onclick=\"ajax('/RESET')\">RESET</button><br><br>";
  message += "<script>function ajax(input){var xhttp = new XMLHttpRequest();xhttp.open(\"GET\", input, true);xhttp.send();}function checkboxClicked(cb){if(cb.checked){ajax(\"/STANDALONE?on=1\")}else{ajax(\"/STANDALONE?on=0\")}};function checkboxSoundClicked(cb){if(cb.checked){ajax(\"/SOUND?on=1\")}else{ajax(\"/SOUND?on=0\")}}</script>";
  server.send(200, "text/html", message);
}


void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleStartBomb(){
  Serial.println("forward");
  server.send(200, "text/plain", "start send");
}

void handleStopBomb(){
  Serial.println("backward");
  server.send(200, "text/plain", "stop send");
}

void handleRight(){
  Serial.println("right");
  server.send(200, "text/plain", "right send");
}

void handleLeft(){
  Serial.println("left");
  server.send(200, "text/plain", "left send");
}

void handleUp(){
  Serial.println("up");
  server.send(200, "text/plain", "up send");
}

void handleDown(){
  Serial.println("down");
  server.send(200, "text/plain", "down send");
}

void handleBeep(){
  int value = getArgValue("note");
  int value2 = getArgValue("length");
  String note = String(value);
  String notelength = String(value2);
  Serial.println("beep "+ note + " " + notelength);
  server.send(200, "text/plain","beep send" );
}

/*
void handleResetBomb(){
  Serial.println("/TIME 0300");
  server.send(200, "text/plain", "reset send");
}

void handleSetTimeBomb(){
  int value = getArgValue("time");
  String myString = String(value);
  Serial.println("/TIME "+ myString);
  server.send(200, "text/plain","time set" );
}

void handleStandaloneBomb(){
  int value = getArgValue("on");
  String myString = String(value);
  Serial.println("/STANDALONE "+ myString);
  server.send(200, "text/plain","time set" );
}

void handleSoundBomb(){
  int value = getArgValue("on");
  String myString = String(value);
  Serial.println("/SOUND "+ myString);
  server.send(200, "text/plain","time set" );
}


void handleEchoBomb(){
  Serial.println("/ECHO");
  server.send(200, "text/plain",lastResponseFromEcho );
}
*/

int getArgValue(String name)
{
  for (uint8_t i = 0; i < server.args(); i++)
    if(server.argName(i) == name)
      return server.arg(i).toInt();
  return -1;
}


String DisplayAddress(IPAddress address)
{
 return String(address[0]) + "." + 
        String(address[1]) + "." + 
        String(address[2]) + "." + 
        String(address[3]);
}



void setup() {
  //Serial1.begin(9600);
  Serial.begin(9600);
  
  //WiFi.softAP(ssid, password); // in case we want to be Access Point
  
  WiFi.begin(ssid, password); // in case we want to be Client
  //WiFi.begin(ssid); // in case we want to be Client w/o password
  //Serial.println("");


  // Wait for connection (WORKS ONLY IF CLIENT)
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  

  //Serial.println("");
  Serial.print("Connected to ");
  Serial.print(ssid);
  Serial.print(" IP address: ");
 
  //IPAddress myIP = WiFi.softAPIP(); // in case we want to be Access Point
  IPAddress myIP = WiFi.localIP(); // in case we want to be Client
  Serial.println(myIP);
  
  redirect = redirect1 + DisplayAddress(myIP) + redirect2; 
  
  //Serial.println(redirect);
  //Serial.println("");
  
  server.on("/", handleRoot);

  //server.on("/STANDALONE", handleStandaloneBomb);

  //server.on("/SOUND", handleSoundBomb);

  server.on("/FORWARD", handleStartBomb);

  server.on("/BACKWARD", handleStopBomb);

  //server.on("/RESET", handleResetBomb);

  //server.on("/TIME", handleSetTimeBomb);

  //server.on("/ECHO", handleEchoBomb);

  server.on("/LEFT", handleLeft);

  server.on("/RIGHT", handleRight);

  server.on("/UP", handleUp);

  server.on("/DOWN", handleDown);

  server.on("/BEEP", handleBeep);

  server.onNotFound(handleNotFound);

  server.begin();
  //Serial.println("HTTP server started");

}

void loop() {
  server.handleClient();
  
  delay(10);
}
