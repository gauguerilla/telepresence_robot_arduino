/*
 * Socket.io <--> WIFI -> Serial Bridge 
 * 
 * from machina eX' Philip Steimel
 * Copresence project 
 * This work was done with funding by the Goethe Institut
 * 
 * www.philipsteimel.de/copresence/
 * www.machinaex.com
 * 
 * based on WebsocketsClient Example from  https://github.com/tzapu/WiFiManager 
 * and
 * Example Sketches from https://github.com/tzapu/WiFiManager
 */

#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>        // https://github.com/tzapu/WiFiManager 


#include <ArduinoJson.h>

#include <WebSocketsClient_Generic.h>
#include <SocketIOclient_Generic.h>

#include <Hash.h>

SocketIOclient socketIO;

#define USE_SERIAL Serial

  // compute the required size
  const int capacity = JSON_ARRAY_SIZE(2)
  + 2*JSON_OBJECT_SIZE(3)
  + 4*JSON_OBJECT_SIZE(1);
  
  // allocate the memory for the document
  StaticJsonDocument<capacity> doc;

  DeserializationError err;

  const char* command;

void socketIOEvent(socketIOmessageType_t type, uint8_t * payload, size_t length) {
//    USE_SERIAL.print("EVENT!!!");

/*  // if we want to get payload into a char array instead of using the reference
    char converted[length];
    USE_SERIAL.println();
    for (int i=0; i<length; i++){
     //USE_SERIAL.print((char) payload[i]);
     converted[i] = (char) payload[i];
     
    }
*/
    /*USE_SERIAL.println();
    USE_SERIAL.println("converted:");
    USE_SERIAL.println(converted);
    */
          
    
    switch(type) {
        case sIOtype_DISCONNECT:
            //USE_SERIAL.printf("[IOc] Disconnected!\n");
            break;
        case sIOtype_CONNECT:
            USE_SERIAL.printf("[IOc] Socketio server connected at url: %s\n", payload);
            // join default namespace (no auto join in Socket.IO V3)
            socketIO.send(sIOtype_CONNECT, "/");
            break;
        case sIOtype_EVENT:
            //USE_SERIAL.printf("[IOc] get event: %s\n", payload);
            //USE_SERIAL.printf(" %s\n", payload); // complete payload
            //USE_SERIAL.println("++++++");
            err = deserializeJson(doc, payload);
        
            // Parse succeeded?
            if (err) {
            Serial.print(F("deserializeJson() returned "));
            Serial.println(err.c_str());
            return;
            }

            if (strcmp(doc[0],"start") == 0){
              command = doc[1]["command"]; // ["socketio event name",{command:"forward"}]  
              USE_SERIAL.println(command);
              USE_SERIAL.println();
            }
            else if(strcmp(doc[0],"end") == 0){
              //command = doc[1]["command"]; // ["socketio event name",{command:"forward"}]
              USE_SERIAL.println("stop");
              USE_SERIAL.println();
            }
            break;
        case sIOtype_ACK:
            //USE_SERIAL.printf("[IOc] get ack: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_ERROR:
            //USE_SERIAL.printf("[IOc] get error: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_EVENT:
            //USE_SERIAL.printf("[IOc] get binary: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_ACK:
            //USE_SERIAL.printf("[IOc] get binary ack: %u\n", length);
            hexdump(payload, length);
            break;
    }
}

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output5State = "off";
String output4State = "off";

// Assign output variables to GPIO pins
const int output5 = 2;
const int output4 = 4;

// WiFiManager
// Local intialization not possible because we use it for reset check.
WiFiManager wifiManager;


void setup() {
  USE_SERIAL.begin(9600);

  
  //Serial.setDebugOutput(true);
  USE_SERIAL.setDebugOutput(true);




   // ADD custom parameter serverIP and serverPort to captcha portal of wifiManager:
   // id/name, placeholder/prompt, default, length
   WiFiManagerParameter custom_serverIP("serverIP", "socket server IP", "192.168.178.50", 64);
   wifiManager.addParameter(&custom_serverIP);
    
  // Uncomment and run it once, if you want to erase all the stored information
  //wifiManager.resetSettings();
  
  // set custom ip for portal
  //wifiManager.setAPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

  // fetches ssid and pass from eeprom and tries to connect
  // if it does not connect it starts an access point with the specified name
  // here  "AutoConnectAP"
  // and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect("Copresence");
  // or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();
  
  // if you get here you have connected to the WiFi
  Serial.println("Connected to wifi.");
  
  server.begin();

   
  
    // server address, port and URL
    //socketIO.begin("192.168.178.116", 5000);
    USE_SERIAL.print("trying to connect to socketio server on ip: ");
    USE_SERIAL.println(custom_serverIP.getValue());
    socketIO.begin(custom_serverIP.getValue(), 5000);
    // event handler
    socketIO.onEvent(socketIOEvent);

  
    // Initialize the output variables as outputs
    pinMode(output5, OUTPUT);
    pinMode(output4, OUTPUT);
    // Set outputs to LOW
    digitalWrite(output5, LOW);
    digitalWrite(output4, LOW);
}

void checkIfReset(){
//Digital pin 1 is pin 5 on the ESP8266
  if (digitalRead(5) == HIGH){
    USE_SERIAL.println("resetting wifi");
    wifiManager.resetSettings(); //resets the stored data for wifi
  }
  
}

unsigned long messageTimestamp = 0;

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  socketIO.loop();

  //checkIfReset();
  
    uint64_t now = millis();
    // send every 2 secs
    /*
        if(now - messageTimestamp > 2000) {
            messageTimestamp = now;
    
            // creat JSON message for Socket.IO (event)
            DynamicJsonDocument doc(1024);
            JsonArray array = doc.to<JsonArray>();
            
            // add evnet name
            // Hint: socket.on('event_name', ....
            array.add("response");
    
            // add payload (parameters) for the event
            JsonObject param1 = array.createNestedObject();
            param1["now"] = (uint32_t) now;
    
            // JSON to String (serializion)
            String output;
            serializeJson(doc, output);
    
            // Send event        
            socketIO.sendEVENT(output);
    
            // Print JSON for debugging
            USE_SERIAL.println(output);
            //USE_SERIAL.println("left");
        }
        */
  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /5/on") >= 0) {
              Serial.println("GPIO 5 on");
              output5State = "on";
              digitalWrite(output5, HIGH);
            } else if (header.indexOf("GET /5/off") >= 0) {
              Serial.println("GPIO 5 off");
              output5State = "off";
              digitalWrite(output5, LOW);
            } else if (header.indexOf("GET /4/on") >= 0) {
              Serial.println("GPIO 4 on");
              output4State = "on";
              digitalWrite(output4, HIGH);
            } else if (header.indexOf("GET /4/off") >= 0) {
              Serial.println("GPIO 4 off");
              output4State = "off";
              digitalWrite(output4, LOW);
            }
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #77878A;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP8266 Web Server</h1>");
            
            // Display current state, and ON/OFF buttons for GPIO 5  
            client.println("<p>GPIO 5 - State " + output5State + "</p>");
            // If the output5State is off, it displays the ON button       
            if (output5State=="off") {
              client.println("<p><a href=\"/5/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/5/off\"><button class=\"button button2\">OFF</button></a></p>");
            } 
               
            // Display current state, and ON/OFF buttons for GPIO 4  
            client.println("<p>GPIO 4 - State " + output4State + "</p>");
            // If the output4State is off, it displays the ON button       
            if (output4State=="off") {
              client.println("<p><a href=\"/4/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/4/off\"><button class=\"button button2\">OFF</button></a></p>");
            }
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
