/*

Telepresence Robot - Drive Wheel Test Code

wheels only: see servo_wheels_serial.ino

Code which tests the forward, backward, right and left
functionality of the telepresence robot base.

*/

#include "pitches.h"

// Include the servo library
#include <Servo.h>

#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

CmdCallback<7> cmdCallback;



// Tell the Arduino there are to continuous servos
Servo ContinuousServo1;  
Servo ContinuousServo2;
Servo ContinuousServo3;

void setup() {
  Serial.begin(9600);

  cmdCallback.addCmd("forward", &cmdForward);
  cmdCallback.addCmd("backward", &cmdBackward);
  cmdCallback.addCmd("right", &cmdRight);
  cmdCallback.addCmd("left", &cmdLeft);
  cmdCallback.addCmd("up", &cmdUp);
  cmdCallback.addCmd("down", &cmdDown);
  cmdCallback.addCmd("beep", &cmdBeep);

  // Attach the continuous servos to pins 6 and 7
  ContinuousServo1.attach(6); 
  ContinuousServo2.attach(7);  
  ContinuousServo3.attach(5);

  // Start the continuous servos in a paused position
  // if they continue to spin slightly, 
  // change these numbers until they stop
  ContinuousServo1.write(90); 
  ContinuousServo2.write(90);
  ContinuousServo3.write(88);

}

void loop() {
  
    CmdBuffer<32> myBuffer;
    CmdParser     myParser;

    Serial.println("Type you commands. Supported: ");
    Serial.println("1: forward");
    Serial.println("2: backward");
    Serial.println("3: right");
    Serial.println("4: left");
    Serial.println("5: up");
    Serial.println("6: down");
    Serial.println("7: beep");

    // Auto Handling
    cmdCallback.loopCmdProcessing(&myParser, &myBuffer, &Serial);
    
    // Pause for a millisecond for stability of the code
    delay(1);
}

void cmdForward(CmdParser *myParser) { Serial.println(">>>");forward();delay(1000);stopDriving(); }
void cmdBackward(CmdParser *myParser) { Serial.println("<<<");backward();delay(1000);stopDriving(); }
void cmdRight(CmdParser *myParser) { Serial.println("RRR");right();delay(1000);stopDriving(); }
void cmdLeft(CmdParser *myParser) { Serial.println("LLL");left();delay(1000);stopDriving(); }
void cmdUp(CmdParser *myParser) { Serial.println("^^^");up();delay(500);stopUpDown(); }
void cmdDown(CmdParser *myParser) { Serial.println("vvv");down();delay(1000);stopUpDown(); }

void cmdBeep(CmdParser *myParser) { 
  Serial.println("beep");
  int freq = atoi(myParser->getCmdParam(1));
  int notelength = atoi(myParser->getCmdParam(2));
  Serial.print(freq);Serial.print(" will be played for ");Serial.println(notelength);
  tone(8, freq, notelength); 
}



// Function to stop UpDown
void stopUpDown(){
  ContinuousServo3.write(88);
}

void up(){
  ContinuousServo3.write(82);
}


void down(){
  ContinuousServo3.write(91);
}


// Function to stop driving
void stopDriving() {
  ContinuousServo1.write(90); 
  ContinuousServo2.write(90); 
}

// Function to drive forwards
void forward(){
  ContinuousServo1.write(70);
  ContinuousServo2.write(102); 
}

// Function to drive backwards
void backward(){
  ContinuousServo1.write(101);
  ContinuousServo2.write(70); 
}

// Function to drive right
void right(){
  ContinuousServo1.write(98);
  ContinuousServo2.write(98); 
}

// Function to drive left
void left(){
  ContinuousServo1.write(75);
  ContinuousServo2.write(75); 
}
