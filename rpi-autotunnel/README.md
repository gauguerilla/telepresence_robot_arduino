# automatic tunneling service on raspberry pi

In this directory you find the config files for a raspberry pi setup that, when booting will start 2 processes:

- a ngrok tunnel and
- the webui_socketIO node server.


ngrok will expose port 5000 to the outside world where node serves the userinterface as a webpage and connects to the robot in the local network.

the relevant lines are 18-34.

## installation


1. install the most recent debian image on the raspi
2. install node, npm, supervisor and configure supervisord:
    ```bash
    curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -;
    sudo apt install nodejs;
    sudo apt-get install supdervisor;
    git clone https://gitlab.com/gauguerilla/telepresence_robot_arduino;
    sudo cp telepresence_robot_arduino/rpi-autotunnel/supervisord.conf /etc/supervisor/supervisord.conf;
    cd telepresence_robot_arduino/webui_socketIO;
    npm i;
    ```
3. install the most recent version of ngrok (ngrok.com) into /bin/, sign up for an account and activate it via token: `ngrok authtoken <YOURAUTHTOKEN>`
4. change the hostname in `telepresence_robot_arduino/rpi-autotunnel/supervisord.conf` line 28 to your ngrok hostname
5. change the hostname in `telepresence_robot_arduino/webui_socketIO/socketioclient.html` line 58 and 63 to your ngrok hostname
6. `sudo reboot`
7. after reboot you can check if supervisord started correctly on startup: `sudo supervisorctl`. You should see something like
    ```
    ngrok-telepresence               RUNNING   pid 451, uptime 0:33:30
    node-server-telepresence         RUNNING   pid 452, uptime 0:33:30
    supervisor> 
    ```

If not: sorry. you gotta do some research into: supdervisor, ngrok and/or node on a raspberry pi
